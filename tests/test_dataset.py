"""Test the pyobsmod.dataset module."""

import numpy as np


from pyobsmod.dataset import Dataset

# Must be improved !!


def test_compute_stats():
    """Test the compute_stats method."""
    obs = np.sin(np.arange(100))
    pred = np.sin(np.arange(100)) * 0.1
    op = Dataset(obs, pred)
    df = op.compute_stats(["bias", "lr", "rmse", "nrmse", "r", "r2"])
    df = df.round(5)
