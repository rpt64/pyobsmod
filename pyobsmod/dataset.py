"""The Dataset module.

Dataset is a class built on pandas DataFrame that allows for quick plotting
and analyzing of observational vs modelled data.

It inherits functionalities from the ``BaseDataset`` class.

"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import pyobsmod.metrics as pym
# import warnings

from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1 import make_axes_locatable
from typing import Any, Literal, Sequence

from pyobsmod.base_dataset import BaseDataset


class Dataset(BaseDataset):
    """Dataset object.

    Dataset is a class built on pandas DataFrames that allows for quick
    plotting and analyzing of observational vs modelled data.

    It inherits functionalities from the ``BaseDataset`` class.

    Parameters
    ----------
    obs : numpy.ndarray | None
        Observations data. Should preferably be a 1-dimensional
        np.ndarray, but lists and tuples can be passed as well.
    mod : list[str] | None
        Modelled data. Should preferably be a 1-dimensional
        np.ndarray, but lists and tuples can be passed as well.
    df : pandas.DataFrame | None
        It is also possible to pass directly a DataFrame that must contain at least
        he columns ``obs`` and ``mod`` and associates string values.
        Note also that if ``df`` is ``None``, both ``obs`` and ``mod`` must not
        be ``None``.
    time : numpy.ndarray | None
        A 1-dimensional array with the time steps of the observation and
        model data. Will be used automatically as ticks in certain plots
        like ``Dataset.time_series_plot``.

    Examples
    --------
    Create a dataset from some data.

    .. jupyter-execute::

        import numpy as np
        import pyobsmod as pom

        obs = np.sin(np.arange(100))
        mod = obs + np.random.normal(size=100)
        ds = pom.Dataset(obs, mod)
        print(ds)

    """

    def __init__(
        self,
        obs: np.ndarray | None = None,
        mod: np.ndarray | None = None,
        df: pd.DataFrame | None = None,
        time: np.ndarray | pd.Index | pd.DatetimeIndex | None = None,
    ) -> None:
        """Initialize the Dataset object.

        Parameters
        ----------
        obs : numpy.ndarray | None
            Observations data. Should preferably be a 1-dimensional
            np.ndarray, but lists and tuples can be passed as well.
        mod : list[str] | None
            Modelled data. Should preferably be a 1-dimensional
            np.ndarray, but lists and tuples can be passed as well.
        df : pandas.DataFrame | None
            It is also possible to pass directly a DataFrame that must contain at least
            the columns ``obs`` and ``mod`` and associates string values.
            Note also that if ``df`` is ``None``, both ``obs`` and ``mod`` must not
            be ``None``.
        time : numpy.ndarray | None
            A 1-dimensional array with the time steps of the observation and
            modelling data. Will be used automatically as ticks in certain plots
            like ``Dataset.time_series_plot``.

        """
        # Initialize BaseDataframe
        super().__init__(obs, mod, df)

        # Initialize time
        if time is not None:
            self.df = self.df.set_index(time)

    def __repr__(self) -> str:
        """Return a string representation of the dataset.

        Returns
        -------
        str
            A string representation of the dataset.

        """
        return "pyobsmod.Dataset(\n" + self.df.__repr__() + "\n)"

    def __add__(self, other: Any) -> "Dataset":
        """Add one or multiple values to dataset or two datasets together.

        Internally the method defers the addition to the underlying pandas
        DataFrame.

        Parameters
        ----------
        other : Any
            The values or other dataset to add.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting sum.

        """
        if isinstance(other, Dataset):
            return Dataset(df=self.df + other.df)
        else:
            return Dataset(df=self.df + other)

    def __radd__(self, other: Any) -> "Dataset":
        """Reverse add one or multiple values to dataset or two datasets together.

        Internally the method defers the addition to the underlying pandas
        DataFrame.

        Parameters
        ----------
        other : Any
            The values or other dataset to add.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting sum.

        """
        return self.__add__(other)

    def __sub__(self, other: Any) -> "Dataset":
        """Subtract one or multiple values to dataset or two datasets together.

        Internally the method defers the subtraction to the underlying pandas
        DataFrame.

        Parameters
        ----------
        other : Any
            The values or other dataset to subtract.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting difference.

        """
        if isinstance(other, Dataset):
            return Dataset(df=self.df - other.df)
        else:
            return Dataset(df=self.df - other)


    def __rsub__(self, other: Any) -> "Dataset":
        """Reverse subtract one or multiple values of dataset or two datasets together.

        Internally the method defers the subtraction to the underlying pandas
        DataFrame.

        Parameters
        ----------
        other : Any
            The values or other dataset to subtract.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting difference.

        """
        return Dataset(df=other - self.df)

    def __mul__(self, other: Any) -> "Dataset":
        """Multiply one or multiple values to dataset or two datasets together.

        Internally the method defers the multiplication to the underlying pandas
        DataFrame. Usually this will result in an element-wise multiplication.

        Parameters
        ----------
        other : Any
            The values or other dataset to multiply.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting product.

        """
        if isinstance(other, Dataset):
            return Dataset(df=self.df * other.df)
        else:
            return Dataset(df=self.df * other)

    def __rmul__(self, other: Any) -> "Dataset":
        """Reverse multiply one or multiple values to dataset or two datasets together.

        Internally the method defers the multiplication to the underlying pandas
        DataFrame. Usually this will result in an element-wise multiplication.

        Parameters
        ----------
        other : Any
            The values or other dataset to multiply.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting product.

        """
        return self.__mul__(other)

    def __truediv__(self, other: Any) -> "Dataset":
        """Divide one or multiple values to dataset or two datasets together.

        Internally the method defers the division to the underlying pandas
        DataFrame. Usually this will result in an element-wise division.

        Parameters
        ----------
        other : Any
            The values or other dataset to divide.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting quotient.

        """
        if isinstance(other, Dataset):
            return Dataset(df=self.df / other.df)
        else:
            return Dataset(df=self.df / other)

    def __rtruediv__(self, other: Any) -> "Dataset":
        """Divide one or multiple values to dataset or two datasets together.

        Internally the method defers the division to the underlying pandas
        DataFrame. Usually this will result in an element-wise division.

        Parameters
        ----------
        other : Any
            The values or other dataset to divide.

        Returns
        -------
        Dataset
            A new Dataset object containing the resulting quotient.

        """
        return Dataset(df=other / self.df)

    def bias(self) -> float:
        """Bias.

        Returns
        -------
        bias : float
            The bias.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.bias())

        """
        return pym.bias(self.obs, self.mod)

    def lr(self) -> tuple[float, float]:
        """Perform linear regression (y=ax+b).

        Using a least squares polynomial fit of degree 1.

        Returns
        -------
        lr : list[float]
            A list with two floats, the slope a and the intercept b.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.lr())

        """
        return pym.lr(self.obs, self.mod)

    def rmse(self) -> float:
        """Root mean squared error.

        Returns
        -------
        rmse : float
            The root mean squared error.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.lr())

        """
        return pym.rmse(self.obs, self.mod)

    def nrmse(
        self,
        norm: Literal["range", "mean"] = "range",
    ) -> float:
        """Normalize root mean squared error.

        Parameters
        ----------
        norm : Literal["range", "mean"]
            The method to normalize the rmse :

                * range : divide by max(y_obs) - min(y_obs)
                * mean : divide by the mean of the observed data

            The default value is "range".

        Returns
        -------
        nrmse : float
            The normalized root mean squared error.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.nrmse())

        """
        return pym.nrmse(self.obs, self.mod, norm=norm)

    def r(
        self,
        method: Literal["pearson", "kendall", "spearman"] = "pearson",
    ) -> float:
        """Correlation coefficient.

        Correlation between sets of data is a
        measure of how well they are related. The most common measure of
        correlation in stats is the Pearson Correlation. It shows the linear
        relationship between two sets of data. In simple terms, it answers the
        question, Can I draw a line graph to represent the data?

        Parameters
        ----------
        method : str
            Method of correlation:

                * pearson : standard correlation coefficient
                * kendall : Kendall Tau correlation coefficient
                * spearman : Spearman rank correlation

            The default value is 'pearson'.

        Returns
        -------
        r : float
            The correlation coefficient.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.r())

        """
        return pym.r(self.obs, self.mod, method=method)

    def r2(
        self,
    ) -> float:
        """Coefficient of determination.

        Returns
        -------
        r2 : float
            The coefficient of determination.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.r2())

        """
        return pym.r2(self.obs, self.mod)

    def compute_stats(
        self,
        which_stats: Sequence[str] | dict[str, Any],
        names: Sequence[str] | None = None,
    ) -> pd.Series:
        """Compute a list of statistics parameters.

        Parameters
        ----------
        which_stats : Sequence[str] | dict[str, Any]
            Sequence of the statistics parameters to compute or alternatively
            a dictionary with the statistics parameters as keys and the arguments
            that are passed to the method as values.
        names : Sequence[str] | None
            Sequence of the names of the statistics parameters. If None, the
            names are taken from the keys of the which_stats dictionary.

        Returns
        -------
        stats : pandas.Series
            A list of the statistic parameters.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.compute_stats(['rmse', 'nrmse']))

        """
        # Check for validity of the input
        if isinstance(which_stats, Sequence):
            stats_dict = {s: {} for s in which_stats}
        elif isinstance(which_stats, dict):
            stats_dict = which_stats
        else:
            raise ValueError("which_stats must be a sequence or a dictionary")

        names = names or stats_dict.keys()
        if len(names) != len(stats_dict):
            raise ValueError("names must have the same length as which_stats")

        params = []
        for func, kwargs in stats_dict.items():
            try:
                stats_func = getattr(self, func)
            except AttributeError as err:
                raise ValueError(
                    "%s is not implemented as a method in %s"
                    % (func, self.__class__.__name__)
                ) from err
            if callable(stats_func):
                params.append(stats_func(**kwargs))
            else:
                raise ValueError(
                    "%s attribute of %s is not callable"
                    % (func, self.__class__.__name__)
                )

        stats = pd.Series(params, names)
        return stats

    def describe_dataset(self) -> pd.Series:
        """Compute the bias, rmse, nrmse, and r2.

        Returns
        -------
        stats : pandas.Series
            A pandas series containing the bias, rmse, nrmse, and r2.

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            print(ds.describe_dataset())

        """
        which_stats = ["bias", "rmse", "nrmse", "r2"]
        stats = self.compute_stats(which_stats)

        return stats

    def __create_textbox(
        self,
        which_stats: Sequence[str] | dict[str, Any],
        names: Sequence[str] | None = None,
        fmt: str | Sequence[str] = ".2f",
        textbox_kws: dict[str, Any] | None = None,
    ) -> AnchoredText:
        """Create statistics textbox for plots.

        Parameters
        ----------
        which_stats : list[str]
            List of statistics to compute.
        names : list[str] | None
            List of labels that are used in the textbox instead of
            ``which_stats``. Has to be in the same order than ``which_stats``.
        fmt : str | Sequence[str]
            Format string for the statistics. Can be a single string that is
            used for all statistics or a sequence of strings that is used for
            each statistic individually.
        textbox_kws : dict[str, Any] | None
            Dictionary that is passed to matplotlibs' ``AnchoredText``.

        Returns
        -------
        anchored_text : matplotlib.offsetbox.AnchoredText
            Textbox with all statistics of ``which_stats``.

        """
        # Use which_stats as labels if names is not passed
        if names is None:
            names = which_stats.copy()

        stats = self.compute_stats(which_stats)
        # Display the computed statistics in a text box using AnchoredText
        text_box_strings = []
        for idx, (stat_item, stat) in enumerate(zip(names, stats)):
            if isinstance(fmt, str):
                text_box_strings.append(f"{stat_item}: {stat:{fmt}}")
            elif isinstance(fmt, Sequence):
                text_box_strings.append(f"{stat_item}: {stat:{fmt[idx]}}")
            else:
                raise ValueError("fmt must be a string or a sequence of strings")

        text_box_content = "\n".join(text_box_strings)

        anchored_text = AnchoredText(text_box_content, **textbox_kws)

        return anchored_text

    def scatter_plot(
        self,
        which_stats: Sequence[str] | dict[str, Any] | None = None,
        names: Sequence[str] | None = None,
        fmt: str | Sequence[str] = ".2f",
        ax: plt.Axes | None = None,
        idline_kws: dict[str, Any] | None = None,
        textbox_kws: dict[str, Any] | None = None,
        **kwargs,
    ) -> plt.Axes:
        """Scatter plot of observed data against modelled data.

        Add an identity line and display selected statistics.

        Parameters
        ----------
        which_stats : Sequence[str] | dict[str, Any] | None
            Sequence of the statistics parameters to compute or alternatively
            a dictionary with the statistics parameters as keys and the arguments
            that are passed to the method as values.
        names : Sequence[str] | None
            Sequence of the names of the statistics parameters. If None, the
            names are taken from the keys of the which_stats dictionary.
        fmt: str | Sequence[str]
            Format string for the statistics. Can be a single string that is
            used for all statistics or a sequence of strings that is used for
            each statistic individually
        ax: plt.Axes | None
            Matplotlib axis to draw the plot on.
        idline_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.pyplot.axline``.
        textbox_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.offsetbox.AnchoredText``.
        **kwargs:
            Additional arguments that are passed to
            ``matplotlib.pyplot.scatter``.

        Returns
        -------
        ax : matplotlib.pyplot.Axes
            The Matplotlib axes.

        Examples
        --------
        .. jupyter-execute::

            import matplotlib.pyplot as plt

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            ds.scatter_plot(['bias', 'rmse', 'nrmse', 'r2'])
            plt.show()

        """
        if ax is None:
            ax = plt.gca()

        # Create scatter plot
        ax.scatter(self.obs, self.mod, **kwargs)

        # Set up default arguments that can be overwritten by dictionary
        idline_kws = {} if idline_kws is None else idline_kws.copy()
        idline_kws.setdefault("lw", 1)
        idline_kws.setdefault("color", "red")

        textbox_kws = {} if textbox_kws is None else textbox_kws.copy()
        textbox_kws.setdefault("loc", "upper left")
        textbox_kws.setdefault("frameon", True)
        textbox_kws.setdefault("borderpad", 1.5)

        # Plot identity line
        obsmin = self.obs.min()
        obsmax = self.obs.max()
        ax.axline((obsmin, obsmin), (obsmax, obsmax), **idline_kws)

        # Set labels and title
        ax.set_title("Observed vs Modelled Data")
        ax.set_xlabel("Observed Data")
        ax.set_ylabel("Modelled Data")

        if which_stats is not None:
            anchored_text = self.__create_textbox(
                which_stats=which_stats,
                names=names,
                fmt=fmt,
                textbox_kws=textbox_kws,
            )

            ax.add_artist(anchored_text)

        return ax

    def scatter_plot_joint(
        self,
        which_stats: Sequence[str] | dict[str, Any] | None = None,
        names: Sequence[str] | None = None,
        fmt: str | Sequence[str] = ".2f",
        nbins: int = 8,
        rfreqs: list[float] = [0, 0.2, 0.4],
        ax: plt.Axes | None = None,
        idline_kws: dict[str, Any] | None = None,
        regline_kws: dict[str, Any] | None = None,
        textbox_kws: dict[str, Any] | None = None,
        **kwargs,
    ) -> tuple[plt.Axes, plt.Axes, plt.Axes]:
        """Scatter plot sns observed data against modelled data.

        Add an identity line and display selected statistics with seaborn's
        jointplot.

        Parameters
        ----------
        which_stats : Sequence[str] | dict[str, Any] | None
            Sequence of the statistics parameters to compute or alternatively
            a dictionary with the statistics parameters as keys and the arguments
            that are passed to the method as values.
        names : Sequence[str] | None
            Sequence of the names of the statistics parameters. If None, the
            names are taken from the keys of the which_stats dictionary.
        fmt: str | Sequence[str]
            Format string for the statistics. Can be a single string that is
            used for all statistics or a sequence of strings that is used for
            each statistic individually
        nbins: int
            Number of bins for the histograms.
        rfreqs: list[float]
            List of relative frequencies for the ticks on the histograms.
        ax: plt.Axes | None
            Matplotlib axis to draw the plot on.
        idline_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.pyplot.axline`` for the
            identity line.
        regline_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.pyplot.axline`` for the
            regression line.
        textbox_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.pyplot.AnchoredText``.
        **kwargs:
            Additional arguments that are passed to ``matplotlib.pyplot.scatter``.

        Returns
        -------
        (ax, ax_histx, ax_histy) : tuple[matplotlib.pyplot.Axes]
            The Matplotlib axes.

        Examples
        --------
        .. jupyter-execute::

            import matplotlib.pyplot as plt

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            axs = ds.scatter_plot_joint(['bias', 'rmse', 'nrmse', 'r2'])

            # You can (but don't have to) access the individual axes
            ax, ax_histx, ax_histy = axs

            plt.show()

        """
        if ax is None:
            ax = plt.gca()

        # Set up default arguments that can be overwritten by dictionary
        idline_kws = {} if idline_kws is None else idline_kws.copy()
        idline_kws.setdefault("lw", 1)
        idline_kws.setdefault("color", "black")

        regline_kws = {} if regline_kws is None else regline_kws.copy()
        regline_kws.setdefault("lw", 1)
        regline_kws.setdefault("ls", "--")
        regline_kws.setdefault("color", "red")

        textbox_kws = {} if textbox_kws is None else textbox_kws.copy()
        textbox_kws.setdefault("loc", "upper left")
        textbox_kws.setdefault("frameon", True)
        textbox_kws.setdefault("borderpad", 1.5)

        # Compute overall min and max
        xymin = min(self.obs.min(), self.mod.min())
        xymax = max(self.obs.max(), self.obs.max())

        # Create scatter plot
        ax.scatter(self.obs, self.mod, **kwargs, label="Data")

        # Plot identity line
        obsmin = self.df.obs.min()
        obsmax = self.df.obs.max()
        ax.axline(
            (obsmin, obsmin), (obsmax, obsmax), label="Identity line", **idline_kws
        )

        # Plot linear regression line
        a, b = self.lr()
        ax.axline((0, b), slope=a, label="Regression line", **regline_kws)

        ax.set_aspect(1.)

        # Create new axes on the right and on the top of the current axes
        divider = make_axes_locatable(ax)
        ax_histx = divider.append_axes("top", 0.8, pad=0.1, sharex=ax)
        ax_histy = divider.append_axes("right", 0.8, pad=0.1, sharey=ax)

        # Make some labels invisible
        ax_histx.xaxis.set_tick_params(labelbottom=False)
        ax_histy.yaxis.set_tick_params(labelleft=False)

        weights = np.ones_like(self.obs) / len(self.obs)

        ax_histx.hist(self.obs, bins=nbins, orientation="vertical", color="lightblue",
                      edgecolor="black", linewidth=1.2, weights=weights)
        ax_histy.hist(self.mod, bins=nbins, orientation="horizontal",
                      color="lightblue", edgecolor="black", linewidth=1.2,
                      weights=weights)

        ax_histx.set_ylabel("Relative\nFrequency [%]")
        ax_histy.set_xlabel("Relative\nFrequency [%]")

        ax_histx.set_yticks(rfreqs)
        ax_histy.set_xticks(rfreqs)

        # Set labels and title
        ax_histx.set_title("Observed vs Modelled Data")
        ax.set_xlabel("Observed Data")
        ax.set_ylabel("Modelled Data")

        # Set limits
        ax.set_xlim(xymin, xymax)
        ax.set_ylim(xymin, xymax)

        sns.despine()

        if which_stats is not None:
            anchored_text = self.__create_textbox(
                which_stats=which_stats,
                names=names,
                fmt=fmt,
                textbox_kws=textbox_kws,
            )

            ax.add_artist(anchored_text)

        return (ax, ax_histx, ax_histy)


    def scatter_plot_sns(
        self,
        which_stats: Sequence[str] | dict[str, Any] | None = None,
        names: Sequence[str] | None = None,
        fmt: str | Sequence[str] = ".2f",
        idline_kws: dict[str, Any] | None = None,
        textbox_kws: dict[str, Any] | None = None,
        **kwargs,
    ) -> sns.JointGrid:
        """Scatter plot sns observed data against modelled data.

        Add an identity line and display selected statistics with seaborn's
        jointplot.

        Parameters
        ----------
        which_stats : Sequence[str] | dict[str, Any] | None
            Sequence of the statistics parameters to compute or alternatively
            a dictionary with the statistics parameters as keys and the arguments
            that are passed to the method as values.
        names : Sequence[str] | None
            Sequence of the names of the statistics parameters. If None, the
            names are taken from the keys of the which_stats dictionary.
        fmt: str | Sequence[str]
            Format string for the statistics. Can be a single string that is
            used for all statistics or a sequence of strings that is used for
            each statistic individually.
        idline_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.pyplot.axline``.
        textbox_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.pyplot.AnchoredText``.
        **kwargs:
            Additional arguments that are passed to ``sns.jointplot``.

        Returns
        -------
        grid : seaborn.JointGrid
            Seaborn JointGrid.

        Notes
        -----
        Note that in contrast to ``Dataset.scatter_plot`` and
        ``Dataset.time_series_plot`` this method does NOT take a
        ``matplotlib.pyplot.Axes`` object as an argument, since the underlying
        function creates a figure and several axes objects itself.

        In practice this means simply, that this plot can NOT be used
        in subplots (without major work-arounds).

        Examples
        --------
        .. jupyter-execute::

            import matplotlib.pyplot as plt

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            grid = ds.scatter_plot_sns(['bias', 'rmse', 'nrmse', 'r2'])

            # You can (but don't have to) access the figure and axes
            fig = grid.fig
            ax = grid.ax_joint
            plt.show()

        """
        # Set up default arguments that can be overwritten by dictionary
        idline_kws = {} if idline_kws is None else idline_kws.copy()
        idline_kws.setdefault("lw", 1)
        idline_kws.setdefault("color", "black")

        textbox_kws = {} if textbox_kws is None else textbox_kws.copy()
        textbox_kws.setdefault("loc", "upper left")
        textbox_kws.setdefault("frameon", True)
        textbox_kws.setdefault("borderpad", 1.5)

        kwargs.setdefault("kind", "reg")
        kwargs.setdefault("height", 10)
        kwargs.setdefault(
            "joint_kws", dict(line_kws=dict(color="red", label="Regression"))
        )

        # Create scatter plot
        grid = sns.jointplot(
            data=self.df, x="obs", y="mod", label="Model Data", **kwargs
        )
        ax = grid.ax_joint

        # Plot identity line
        obsmin = self.df.obs.min()
        obsmax = self.df.obs.max()
        ax.axline(
            (obsmin, obsmin), (obsmax, obsmax), label="Identity line", **idline_kws
        )

        # Set labels and title
        ax.set_title("Observed vs Modelled Data")
        ax.set_xlabel("Observed Data")
        ax.set_ylabel("Modelled Data")

        if which_stats is not None:
            anchored_text = self.__create_textbox(
                which_stats=which_stats,
                names=names,
                fmt=fmt,
                textbox_kws=textbox_kws,
            )

            ax.add_artist(anchored_text)

        return grid

    def stats_plot(
        self,
        which_stats: Sequence[str] | dict[str, Any],
        names: Sequence[str] | None = None,
        decimals: int = 2,
        fontsize: float = 10,
        xscale: float = 0.2,
        yscale: float = 1,
        ax: plt.Axes | None = None,
        **kwargs,
    ) -> plt.Axes:
        """Textbox plot summarizing specified statistics.

        Parameters
        ----------
        which_stats : Sequence[str] | dict[str, Any]
            Sequence of the statistics parameters to compute or alternatively
            a dictionary with the statistics parameters as keys and the arguments
            that are passed to the method as values.
        names : Sequence[str] | None
            Sequence of the names of the statistics parameters. If None, the
            names are taken from the keys of the which_stats dictionary.
        decimals : int
            Number of decimal places to display for statistics. Defaults to 2.
        fontsize : float | None
            Font size for the text in the textbox plot. Defaults to 10.
        xscale : float | None
            Scales the width of the textbox plot. Defaults to 0.5.
        yscale : float | None
            Scales the height of the textbox plot. Defaults to 1.
        ax : matplotlib.pyplot.Axes | None
            Matplotlib axis to draw the plot on. If None, a new axis
            is created.
        **kwargs
            Additional arguments passed to `matplotlib.pyplot.table` for
            table customization.

        Returns
        -------
        ax : matplotlib.pyplot.Axes
            The Matplotlib axis object containing the textbox plot.

        Examples
        --------
        .. jupyter-execute::

            import matplotlib.pyplot as plt

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            ds.stats_plot(['bias', 'rmse', 'nrmse', 'r2'])
            plt.show()

        """
        # Create ax if needed
        if ax is None:
            ax = plt.gca()

        # Compute stats
        df = self.compute_stats(which_stats)

        # Change labels
        if names is not None:
            df.index = names

        # Format the values with number of decimal places
        df = df.apply(lambda x: f"{x:.{decimals}f}")

        # Treat kwargs
        if "loc" not in kwargs:
            kwargs["loc"] = "center"

        # Create a table from DataFrame
        table = ax.table(
            cellText=df.values.reshape(-1, 1),
            rowLabels=df.index,
            fontsize=fontsize,
            **kwargs,
        )

        # Scale the table
        table.scale(xscale, yscale)

        # Hide axes
        ax.axis("off")

        return ax

    def time_series_plot(
        self,
        which_stats: Sequence[str] | dict[str, Any] | None = None,
        names: Sequence[str] | None = None,
        fmt: str | Sequence[str] = ".2f",
        ax: plt.Axes | None = None,
        textbox_kws: dict[str, Any] | None = None,
        obs_kws: dict[str, Any] | None = None,
        mod_kws: dict[str, Any] | None = None,
        **kwargs,
    ) -> plt.Axes:
        """Time series plot of observed data against modelled data.

        Display selected statistics.

        Parameters
        ----------
        which_stats : Sequence[str] | dict[str, Any] | None
            Sequence of the statistics parameters to compute or alternatively
            a dictionary with the statistics parameters as keys and the arguments
            that are passed to the method as values.
        names : Sequence[str] | None
            Sequence of the names of the statistics parameters. If None, the
            names are taken from the keys of the which_stats dictionary.
        fmt: str | Sequence[str]
            Format string for the statistics. Can be a single string that is
            used for all statistics or a sequence of strings that is used for
            each statistic individually.
        ax: matplotlib.pyplot.Axes | None
            Matplotlib axis to draw the plot on.
        textbox_kws: dict[str, Any] | None
            Dictionary that is passed to ``matplotlib.offsetbox.AnchoredText``.
        obs_kws: dict[str, Any] | None
            Dictionary that is passed to the observation data
            ``matplotlib.pyplot.plot`` call.
        mod_kws: dict[str, Any] | None
            Dictionary that is passed to the modelled data
            ``matplotlib.pyplot.plot`` call.
        **kwargs:
            Additional arguments that are passed to both
            ``matplotlib.pyplot.plot`` calls.

        Returns
        -------
        ax : matplotlib.pyplot.Axes
            The Matplotlib axes.

        Examples
        --------
        .. jupyter-execute::

            import matplotlib.pyplot as plt

            from pyobsmod import load_dataset_example

            ds = load_dataset_example()
            ds.time_series_plot(['bias', 'rmse', 'nrmse', 'r2'])
            plt.show()

        """
        # Create ax if needed
        if ax is None:
            ax = plt.gca()

        # Create time
        time = self.df.index.values

        # Update obs_ and mod_kws keyword arguments if necessary
        if len(kwargs):
            obs_kws.update(kwargs)
            mod_kws.update(kwargs)

        obs_kws = {} if obs_kws is None else obs_kws.copy()
        obs_kws.setdefault("label", "Observed")

        mod_kws = {} if mod_kws is None else mod_kws.copy()
        mod_kws.setdefault("label", "Modelled")

        textbox_kws = {} if textbox_kws is None else textbox_kws.copy()
        textbox_kws.setdefault("loc", "upper left")
        textbox_kws.setdefault("frameon", True)
        textbox_kws.setdefault("borderpad", 1.5)

        ax.plot(time, self.obs, **obs_kws)
        ax.plot(time, self.mod, **mod_kws)

        # Set labels and title
        ax.set_title("Time Series: Observed vs Modelled")
        ax.set_xlabel("Time")
        ax.set_ylabel("Values")
        ax.legend()

        if which_stats is not None:
            anchored_text = self.__create_textbox(
                which_stats=which_stats,
                names=names,
                fmt=fmt,
                textbox_kws=textbox_kws,
            )

            ax.add_artist(anchored_text)

        return ax
