"""The BaseDataset module.

BaseDataset is a class for storing observational and modeled data.
It can be initialized with either a single NumPy array containing
both observations and model data, or a pandas DataFrame with separate
columns named 'obs' and 'mod'.

"""

import numpy as np
import pandas as pd

from pathlib import Path
from typing import Any


class BaseDataset:
    """BaseDataset object.

    BaseDataset is a class for storing observational and modelled data.
    It can be initialized with either a single NumPy array containing
    both observations and model data, or a pandas DataFrame with separate
    columns named 'obs' and 'mod'.

    Parameters
    ----------
    obs : numpy.ndarray | None
        Observation data. Should preferably be a 1-dimensional numpy.ndarray,
        but lists and tuples can be passed as well.
    mod : numpy.ndarray | None
        Modelled data. Should preferably be a 1-dimensional
        np.ndarray, but lists and tuples can be passed as well.
    df : pandas.DataFrame | None
        Instead of passing two numpy arrays, it is also possible to pass
        directly a DataFrame that must contain at least the columns ``obs``
        and ``mod``. Note also that if ``df`` is ``None``, both ``obs`` and
        ``mod`` must not be ``None``.

    """

    def __init__(
        self,
        obs: np.ndarray | list | tuple | None = None,
        mod: np.ndarray | list | tuple | None = None,
        df: pd.DataFrame | None = None,
    ) -> None:
        """Initialize the BaseDataset object.

        Parameters
        ----------
        obs : numpy.ndarray
            Observations data.
        mod : numpy.ndarray
            Modelled data.
        df : pandas.DataFrame
            DataFrame containing the dataset.
            If provided, `obs` and `mod` will be ignored.

        """
        # Initialize dataframe
        if df is not None:
            self.df = df
        elif obs is not None and mod is not None:
            self.df = pd.DataFrame(dict(obs=obs, mod=mod))
        else:
            raise ValueError("Either both obs and mod, or df must be not None.")

    def __repr__(self) -> str:
        """Return a string representation of the dataset.

        Returns
        -------
        str
            A string representation of the dataset.

        """
        return "pyobsmod.Dataset(\n" + self.df.__repr__() + "\n)"

    def __len__(self) -> int:
        """Get the number of rows in the dataset.

        Returns
        -------
        int
            The number of rows in the dataset.

        """
        return len(self.df)

    def __contains__(self, item: str) -> bool:
        """Check if a column is contained in the dataset.

        Parameters
        ----------
        item : str
            The name of the column to check.

        Returns
        -------
        bool
            True if the column is contained in the dataset, False otherwise.

        """
        if not isinstance(item, str):
            raise ValueError(f"{item} must be a column of the underlying dataframe")
        return item in self.df.columns

    def __getitem__(self, position: slice | int) -> "BaseDataset":
        """Get a subset of the dataset.

        Parameters
        ----------
        position : slice or int
            The position or slice of rows to retrieve.

        Returns
        -------
        Dataset
            A new BaseDataset object containing the subset of the dataset.

        """
        if not isinstance(position, slice):
            position = [position]
        return BaseDataset(df=self.df.iloc[position])

    def __getattr__(self, name: str) -> np.ndarray:
        """Get a column of the dataset as an attribute.

        Parameters
        ----------
        name : str
            The name of the column.

        Returns
        -------
        numpy.ndarray
            The values of the specified column.

        Raises
        ------
        AttributeError
            If the specified column is not found in the dataset.

        """
        # __getattr__ is only called if no class attributes match 'name'
        if name in self:
            return self.df[name].values

        # Fall back to default behaviour (and error) otherwise
        return super().__getattribute__(name)

    def __setattr__(self, name: str, value: Any):
        """Set attributes of the dataset.

        Parameters
        ----------
        name : str
            The name of the attribute.
        value : Any
            The value to set the attribute to.

        """
        # Allows users to set obs and mod without accessing the dataframe
        if name in {"obs", "mod"}:
            self.df[name] = value

        # Fall back to default behaviour (and error) otherwise
        super().__setattr__(name, value)

    @property
    def df(self) -> pd.DataFrame:
        """Retrieves the DataFrame stored in the object.

        Returns
        -------
        pandas.DataFrame
            The DataFrame stored in the object.

        """
        return self._df

    @df.setter
    def df(self, new_df: pd.DataFrame):
        """Set the underlying DataFrame containing the dataset.

        Parameters
        ----------
        new_df : pandas.DataFrame
            The new DataFrame to set.

        Raises
        ------
        TypeError
            If ``new_df`` is not a pandas DataFrame.
        ValueError
            If the DataFrame does not contain the columns 'obs' and 'mod'.

        """
        # Check if object is a dataframe with (at least) columns obs and mod
        if not isinstance(new_df, pd.DataFrame):
            raise TypeError("``new_df`` must be a pandas DataFrame.")
        if not {"obs", "mod"}.issubset(new_df.columns):
            raise ValueError("Dataframe must contain columns ``obs`` " "and ``mod``.")

        self._df = new_df

    @property
    def values(self) -> np.ndarray:
        """Get the dataset as a numpy array.

        Returns
        -------
        numpy.ndarray
            The dataset as a numpy array.

        """
        return self._df.values

    @property
    def data(self) -> np.ndarray:
        """Alias for ``self.values``.

        Returns
        -------
        numpy.ndarray
            The dataset as a numpy array.

        """
        return self.values

    def save(self, path: str | Path) -> None:
        """Save this class as a pickle file.

        Internally only the pandas dataframe is saved and the class can
        be reconstructed from that dataframe. If needed then, the pickle
        file can be loaded directly in pandas with ``pd.read_pickle``.

        Parameters
        ----------
        path : str | Path
            File path where the pickled object will be stored.

        """
        self.df.to_pickle(path)


def load_dataset(path: str | Path) -> BaseDataset:
    """Load ``Dataset`` from a pickled dataframe.

    The file to load did not have to be created by ``BaseDataset.save``
    and can come directly from a saved dataframe ``pandas.to_pickle``. However,
    the dataframe still must contain the ``obs`` and ``mod`` columns.

    Parameters
    ----------
    path : str | Path
        File path where the pickled object will be stored.

    Returns
    -------
    BaseDataset
        BaseDataset with observation and model columns.

    """
    return BaseDataset(df=pd.read_pickle(path))
