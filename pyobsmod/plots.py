"""Plotting module.

Contains the same plotting functions as defined in
the ``pyobsmod.Dataset`` methods. Functions have the advantage that no
class instance has to be created. However, internally all functions create
a class instance and refer to their respective class methods.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from pyobsmod.dataset import Dataset
from typing import Any, Sequence


def scatter_plot(
    obs: np.ndarray,
    mod: np.ndarray,
    which_stats: Sequence[str] | dict[str, Any] | None = None,
    names: Sequence[str] | None = None,
    fmt: str | Sequence[str] = ".2f",
    ax: plt.Axes | None = None,
    idline_kws: dict[str, Any] | None = None,
    textbox_kws: dict[str, Any] | None = None,
    **kwargs,
) -> plt.Axes:
    """Scatter plot of observed data against modelled data.

    Add an identity line and display selected statistics.

    Internally this function create a Dataset object and passes the arguments
    to the ``Dataset.scatter_plot`` method.

    Parameters
    ----------
    obs: numpy.ndarray
        1d numpy array that contains the data that was observed / measured.
    mod: numpy.ndarray
        1d numpy array that contains the data that was predicted / modelled.
    which_stats : Sequence[str] | dict[str, Any] | None
        Sequence of the statistics parameters to compute or alternatively
        a dictionary with the statistics parameters as keys and the arguments
        that are passed to the method as values.
    names : Sequence[str] | None
        Sequence of the names of the statistics parameters. If None, the
        names are taken from the keys of the which_stats dictionary.
    fmt: str | Sequence[str]
        Format string for the statistics. Can be a single string that is
        used for all statistics or a sequence of strings that is used for
    ax: matplotlib.pyplot.Axes
        Matplotlib axis to draw the plot on.
    idline_kws: dict[str, Any]
        Dictionary that is passed to ``matplotlib.pyplot.axline``.
    textbox_kws: dict[str, Any]
        Dictionary that is passed to ``matplotlib.offsetbox.AnchoredText``.
    **kwargs:
        Additional arguments that are passed to ``matplotlib.pyplot.scatter``.

    Returns
    -------
    ax : matplotlib.pyplot.Axes
        The Matplotlib axes.

    Examples
    --------
    .. jupyter-execute::

        import matplotlib.pyplot as plt
        import numpy as np
        import pyobsmod.plots as pp

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        mod = np.sin(np.arange(100)) + np.random.normal(size=100)

        ax = pp.scatter_plot(obs, mod, ['bias', 'rmse', 'nrmse', 'r2'])
        plt.show()

    """
    __checkarrays([obs, mod])
    op = Dataset(obs, mod)
    ax = op.scatter_plot(
        which_stats=which_stats,
        names=names,
        fmt=fmt,
        ax=ax,
        idline_kws=idline_kws,
        textbox_kws=textbox_kws,
        **kwargs,
    )

    return ax


def scatter_plot_joint(
    obs: np.ndarray,
    mod: np.ndarray,
    which_stats: Sequence[str] | dict[str, Any] | None = None,
    names: Sequence[str] | None = None,
    fmt: str | Sequence[str] = ".2f",
    nbins: int = 8,
    rfreqs: list[float] = [0, 0.2, 0.4],
    ax: plt.Axes | None = None,
    idline_kws: dict[str, Any] | None = None,
    regline_kws: dict[str, Any] | None = None,
    textbox_kws: dict[str, Any] | None = None,
    **kwargs,
) -> tuple[plt.Axes, plt.Axes, plt.Axes]:
    """Scatter plot sns observed data against modelled data.

    Add an identity line and display selected statistics with seaborn's
    jointplot.

    Internally this function create a Dataset object and passes the arguments
    to the ``Dataset.scatter_plot_joint`` method.

    Parameters
    ----------
    obs: numpy.ndarray
        1d numpy array that contains the data that was observed / measured.
    mod: numpy.ndarray
        1d numpy array that contains the data that was predicted / modelled.
    which_stats : Sequence[str] | dict[str, Any] | None
        Sequence of the statistics parameters to compute or alternatively
        a dictionary with the statistics parameters as keys and the arguments
        that are passed to the method as values.
    names : Sequence[str] | None
        Sequence of the names of the statistics parameters. If None, the
        names are taken from the keys of the which_stats dictionary.
    fmt: str | Sequence[str]
        Format string for the statistics. Can be a single string that is
        used for all statistics or a sequence of strings that is used for
    nbins: int
        Number of bins for the histograms.
    rfreqs: list[float]
        List of relative frequencies for the ticks on the histograms.
    ax: plt.Axes | None
        Matplotlib axis to draw the plot on.
    idline_kws: dict[str, Any] | None
        Dictionary that is passed to ``matplotlib.pyplot.axline`` for the
        identity line.
    regline_kws: dict[str, Any] | None
        Dictionary that is passed to ``matplotlib.pyplot.axline`` for the
        regression line.
    textbox_kws: dict[str, Any] | None
        Dictionary that is passed to ``matplotlib.pyplot.AnchoredText``.
    **kwargs:
        Additional arguments that are passed to ``matplotlib.pyplot.scatter``.

    Returns
    -------
    axs : tuple[matplotlib.pyplot.Axes, matplotlib.pyplot.Axes, matplotlib.pyplot.Axes]
        The matplotlib axes of the scatter plot and both histograms.

    Examples
    --------
    .. jupyter-execute::

        import matplotlib.pyplot as plt
        import numpy as np
        import pyobsmod.plots as pp

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        mod = np.sin(np.arange(100)) + np.random.normal(size=100)

        axs = pp.scatter_plot_joint(obs, mod, ['bias', 'rmse', 'nrmse', 'r2'])
        # You can (but don't have to) access the individual axes
        ax, ax_histx, ax_histy = axs
        plt.show()

    """
    __checkarrays([obs, mod])
    op = Dataset(obs, mod)
    axs = op.scatter_plot_joint(
        which_stats=which_stats,
        names=names,
        fmt=fmt,
        nbins=nbins,
        rfreqs=rfreqs,
        ax=ax,
        idline_kws=idline_kws,
        regline_kws=regline_kws,
        textbox_kws=textbox_kws,
        **kwargs,
    )

    return axs


def scatter_plot_sns(
    obs: np.ndarray,
    mod: np.ndarray,
    which_stats: Sequence[str] | dict[str, Any] | None = None,
    names: Sequence[str] | None = None,
    fmt: str | Sequence[str] = ".2f",
    idline_kws: dict[str, Any] | None = None,
    textbox_kws: dict[str, Any] | None = None,
    **kwargs,
) -> sns.JointGrid:
    """Scatter plot sns of observed data against modelled data.

    Add an identity line and display selected statistics with seaborn's jointplot.

    Internally this function create a Dataset object and passes the arguments
    to the ``Dataset.scatter_plot_sns`` method.

    Parameters
    ----------
    obs: numpy.ndarray
        1d numpy array that contains the data that was observed / measured.
    mod: numpy.ndarray
        1d numpy array that contains the data that was predicted / modelled.
    which_stats : Sequence[str] | dict[str, Any] | None
        Sequence of the statistics parameters to compute or alternatively
        a dictionary with the statistics parameters as keys and the arguments
        that are passed to the method as values.
    names : Sequence[str] | None
        Sequence of the names of the statistics parameters. If None, the
        names are taken from the keys of the which_stats dictionary.
    fmt: str | Sequence[str]
        Format string for the statistics. Can be a single string that is
        used for all statistics or a sequence of strings that is used for
    idline_kws: dict[str, Any] | None
        Dictionary that is passed to ``matplotlib.pyplot.axline``.
    textbox_kws: dict[str, Any] | None
        Dictionary that is passed to ``matplotlib.offsetbox.AnchoredText``.
    **kwargs:
        Additional arguments that are passed to ``seaborn.jointplot``.

    Returns
    -------
    grid : seaborn.JointGrid
        An instance of a seaborn JointGrid.

    Notes
    -----
    Note that in contrast to ``plots.scatter_plot`` and
    ``plots.time_series_plot`` this method does NOT take a ``matplotlib.pyplot.Axes``
    object as an argument, since the underlying function creates
    a figure and several axes objects itself.

    In practice this means simply, that this plot can NOT be used
    in subplots (without major work-arounds).

    Examples
    --------
    .. jupyter-execute::

        import matplotlib.pyplot as plt
        import numpy as np
        import pyobsmod.plots as pp

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        mod = np.sin(np.arange(100)) + np.random.normal(size=100)
        g = pp.scatter_plot_sns(obs, mod, ['bias', 'rmse', 'nrmse', 'r2'])
        plt.show()

    """
    __checkarrays([obs, mod])
    op = Dataset(obs, mod)
    grid = op.scatter_plot_sns(
        which_stats=which_stats,
        names=names,
        fmt=fmt,
        idline_kws=idline_kws,
        textbox_kws=textbox_kws,
        **kwargs,
    )

    return grid


def time_series_plot(
    obs: np.ndarray,
    mod: np.ndarray,
    time: np.ndarray | pd.Index | pd.DatetimeIndex | None = None,
    which_stats: Sequence[str] | dict[str, Any] | None = None,
    names: Sequence[str] | None = None,
    fmt: str | Sequence[str] = ".2f",
    ax: plt.Axes | None = None,
    textbox_kws: dict[str, Any] | None = None,
    obs_kws: dict[str, Any] | None = None,
    mod_kws: dict[str, Any] | None = None,
    **kwargs,
) -> plt.Axes:
    """Time series plot of observed data against modelled data.

    Display selected statistics.

    Internally this function create a Dataset object and passes the arguments
    to the ``Dataset.scatter_plot`` method.

    Parameters
    ----------
    obs : numpy.ndarray
        Observation data. Should preferably be a 1-dimensional numpy.ndarray, but
        lists and tuples can be passed as well.
    mod : numpy.ndarray
        Predicted / modelled data. Should preferably be a 1-dimensional
        numpy.ndarray, but lists and tuples can be passed as well.
    time : numpy.ndarray | pandas.Index | pandas.DatetimeIndex | None
        A 1-dimensional array with the time steps of the observation and
        modelled data. Will be used automatically as ticks in certain plots
        like ``Dataset.time_series_plot``.
    which_stats : Sequence[str] | dict[str, Any] | None
        Sequence of the statistics parameters to compute or alternatively
        a dictionary with the statistics parameters as keys and the arguments
        that are passed to the method as values.
    names : Sequence[str] | None
        Sequence of the names of the statistics parameters. If None, the
        names are taken from the keys of the which_stats dictionary.
    fmt: str | Sequence[str]
        Format string for the statistics. Can be a single string that is
        used for all statistics or a sequence of strings that is used for
    ax: matplotlib.pyplot.Axes | None
        Matplotlib axis to draw the plot on.
    textbox_kws: dict[str, Any] | None
        Dictionary that is passed to ``matplotlib.offsetbox.AnchoredText``.
    obs_kws: dict[str, Any] | None
        Dictionary that is passed to the observation data ``matplotlib.pyplot.plot``
        call.
    mod_kws: dict[str, Any] | None
        Dictionary that is passed to the modelled data ``matplotlib.pyplot.plot``
        call.
    **kwargs:
        Additional arguments that are passed to both ``matplotlib.pyplot.plot`` calls.

    Returns
    -------
    ax : matplotlib.pyplot.Axes
        The Matplotlib axes.

    Examples
    --------
    .. jupyter-execute::

        import matplotlib.pyplot as plt
        import numpy as np
        import pyobsmod.plots as pp

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        mod = np.sin(np.arange(100)) + np.random.normal(size=100)
        ax = pp.time_series_plot(
                obs, mod, which_stats=['bias', 'rmse', 'nrmse', 'r2']
            )
        plt.show()

    """
    __checkarrays([obs, mod])

    op = Dataset(obs, mod, time=time)
    ax = op.time_series_plot(
        which_stats=which_stats,
        names=names,
        fmt=fmt,
        ax=ax,
        textbox_kws=textbox_kws,
        obs_kws=obs_kws,
        mod_kws=mod_kws,
        **kwargs,
    )

    return ax


def __checkarrays(arrs: np.ndarray) -> None:
    """Check arrays.

    Check if arrays are valid numpy arrays and have appropriate dimensions.

    Parameters
    ----------
    arrs : numpy.ndarray
        An array or sequence of arrays to be checked.

    Raises
    ------
    ValueError
        If an array is not a valid numpy array or has more than one dimension.
        If the data cannot be converted to a numpy array.

    Notes
    -----
    This function is intended to check if the provided arrays are valid numpy arrays
    with one dimension. If an array is not 1-dimensional, it raises an error.
    If an array is not a numpy array, it attempts to convert it to one.
    If conversion is not possible, it raises an error.

    """
    for arr in arrs:
        if not isinstance(arr, np.ndarray):
            try:
                arr = np.array(arr)
            except ValueError:
                raise ValueError(
                    "Data can not be converted to numpy array. Check data type"
                )
        if arr.squeeze().ndim > 1:
            raise ValueError("Array is not 1-d, verify the dimensions.")
