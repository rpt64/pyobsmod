"""The metrics module.

A collection of functions to quickly calculate common metrics for
model evaluation.

"""

import numpy as np
import pandas as pd

from sklearn.metrics import r2_score
from typing import Literal, Sequence, Any


def bias(obs: np.ndarray, mod: np.ndarray) -> float:
    """Bias.

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.

    Returns
    -------
    bias : float
        The bias.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.bias(obs, mod))

    """
    return float(np.nanmean(mod - obs))


def lr(obs: np.ndarray, mod: np.ndarray) -> tuple[float, float]:
    """Perform linear regression (y=ax+b).

    Using a least squares polynomial fit of degree 1.

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.

    Returns
    -------
    lr : list[float]
        A list with two floats, the slope a and the intercept b.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.lr(obs, mod))

    """
    a, b = np.polyfit(obs, mod, deg=1)

    return (a, b)


def rmse(obs: np.ndarray, mod: np.ndarray) -> float:
    """Root mean squared error.

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.

    Returns
    -------
    rmse : float
        The root mean squared error.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.rmse(obs, mod))

    """
    return float(np.nanmean((obs - mod) ** 2) ** 0.5)


def nrmse(
    obs: np.ndarray,
    mod: np.ndarray,
    norm: Literal["range", "mean"] = "range",
) -> float:
    """Normalize root mean squared error.

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.
    norm : Literal["range", "mean"]
        The method to normalize the rmse :

            * range : divide by max(y_obs) - min(y_obs)
            * mean : divide by the mean of the observed data

        The default value is "range".

    Returns
    -------
    nrmse : float
        The normalized root mean squared error.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.nrmse(obs, mod))

    """
    if norm == "range":
        return float(rmse(obs, mod) / (np.nanmax(obs) - np.nanmin(obs)))
    elif norm == "mean":
        return float(rmse(obs, mod) / np.nanmean(obs))
    else:
        raise ValueError("'norm' must be one of 'range' or 'mean'.")


def r(
    obs: np.ndarray,
    mod: np.ndarray,
    method: Literal["pearson", "kendall", "spearman"] = "pearson",
) -> float:
    """Correlation coefficient.

    Correlation between sets of data is a
    measure of how well they are related. The most common measure of
    correlation in stats is the Pearson Correlation. It shows the linear
    relationship between two sets of data. In simple terms, it answers the
    question, Can I draw a line graph to represent the data?

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.
    method : str
        Method of correlation:

            * pearson : standard correlation coefficient
            * kendall : Kendall Tau correlation coefficient
            * spearman : Spearman rank correlation

        The default value is 'pearson'.

    Returns
    -------
    r : float
        The correlation coefficient.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.r(obs, mod))

    """
    df = pd.DataFrame({"obs": obs, "mod": mod})
    return float(df.corr(method=method).loc["obs", "mod"])


def r2(obs: np.ndarray, mod: np.ndarray) -> float:
    """Coefficient of determination.

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.

    Returns
    -------
    r2 : float
        The coefficient of determination.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.r(obs, mod))

    """
    mask = ~np.isnan(obs) & ~np.isnan(mod)  # Check for NaNs
    return r2_score(obs[mask], mod[mask])


def compute_stats(
    obs: np.ndarray,
    mod: np.ndarray,
    which_stats: Sequence[str] | dict[str, Any],
    names: Sequence[str] | None = None,
) -> pd.Series:
    """Compute a list of statistics parameters.

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.
    which_stats : Sequence[str] | dict[str, Any]
        Sequence of the statistics parameters to compute or alternatively
        a dictionary with the statistics parameters as keys and the arguments
        that are passed to the method as values.
    names : Sequence[str] | None
        Sequence of the names of the statistics parameters. If None, the
        names are taken from the keys of the which_stats dictionary.

    Returns
    -------
    stats : pandas.Series
        A list of the statistic parameters.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.compute_stats(obs, mod, ['rmse', 'nrmse']))

    """
    # Check for validity of the input
    if isinstance(which_stats, Sequence):
        stats_dict = {s: {} for s in which_stats}
    elif isinstance(which_stats, dict):
        stats_dict = which_stats
    else:
        raise ValueError("which_stats must be a sequence or a dictionary")

    names = names or stats_dict.keys()
    if len(names) != len(stats_dict):
        raise ValueError("names must have the same length as which_stats")

    params = []
    for func, kwargs in stats_dict.items():
        try:
            stats_func = globals()[func]
        except AttributeError as err:
            raise ValueError(
                f"{func} is not implemented as a function"
            ) from err
        if callable(stats_func):
            params.append(stats_func(obs, mod, **kwargs))
        else:
            raise ValueError(
                f"{func} is not a callable function"
            )

    stats = pd.Series(params, names)
    return stats


def describe_dataset(obs: np.ndarray, mod:np.ndarray) -> pd.Series:
    """Compute the bias, rmse, nrmse, and r2.

    Parameters
    ----------
    obs : np.ndarray
        The observed values.
    mod : np.ndarray
        The modeled values.

    Returns
    -------
    stats : pandas.Series
        A pandas series containing the bias, rmse, nrmse, and r2.

    Examples
    --------
    .. jupyter-execute::

        import pyobsmod.metrics as pym

        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        obs, mod = ds.obs, ds.mod
        print(pym.describe_dataset(obs, mod))
        from pyobsmod import load_dataset_example

        ds = load_dataset_example()
        print(ds.describe_dataset())

    """
    which_stats = ["bias", "rmse", "nrmse", "r2"]
    stats = compute_stats(obs, mod, which_stats)

    return stats
