"""The pyobsmod package.

Fundamental framework for the systematic manipulation and visualization of
observed and modelled data.
"""

__version__ = "0.1.8"

from pyobsmod.base_dataset import load_dataset
from pyobsmod.dataset import Dataset
from pyobsmod.categorical_dataset import CategoricalDataset
from pyobsmod.example_dataset import load_dataset_example
from pyobsmod.example_dataset import load_categorical_dataset_example
