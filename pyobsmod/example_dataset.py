"""Example Dataset module.

A collection of simple example datasets for visualization purposes
and for getting started quickly. Enables new users to explore the toolbox
without having to prepare their own datasets.

"""

import numpy as np
import pandas as pd

from pyobsmod import Dataset, CategoricalDataset


def load_dataset_example() -> Dataset:
    """Load a small wave-like example dataset.

    Returns
    -------
        Dataset: The example dataset.

    """
    # Set a random seed to always get the same dataset
    np.random.seed(42)

    t = np.linspace(0, 50, 100)
    time_range = pd.date_range(start='1/1/2018', periods=100, freq='3h')

    # Create some wave-like data
    obs = np.sin(t) * np.sin(1/5*t) + np.random.normal(0, 0.1, t.shape)
    mod = obs + np.random.normal(0, 0.2, t.shape)

    # Create a dataset
    dataset = Dataset(obs=obs, mod=mod, time=time_range)

    return dataset


def load_categorical_dataset_example() -> CategoricalDataset:
    """Load a small categorical example dataset.

    Returns
    -------
        CategoricalDataset: The example dataset.

    """
    obs = ['green', 'orange', 'orange', 'red', 'red', 'red']
    mod = ['green', 'orange', 'orange', 'orange', 'red', 'red']
    labels = ['green', 'orange', 'red']

    return CategoricalDataset(obs, mod, labels)
