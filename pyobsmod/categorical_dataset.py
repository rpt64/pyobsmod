"""The CategoricalDataset module.

CategoricalDataset is a class built on pandas DataFrame that allows for quick plotting
and analyzing of observational vs modelled modelled categorical data.

It inherits functionalities from the ``BaseDataset`` class.

"""

import matplotlib.pyplot as plt
import seaborn as sns

from pyobsmod.base_dataset import BaseDataset
from sklearn.metrics import confusion_matrix


class CategoricalDataset(BaseDataset):
    """CategoricalDataset object.

    CategoricalDataset is a class built on pandas DataFrames that allows for quick
    plotting and analyzing of observational vs modelled categorical data.

    Parameters
    ----------
    obs : list[str] | None
        Observations data.
    mod : list[str] | None
        Model data.
    labels : list[str]
        List of class labels.

    Examples
    --------
    Create a dataset from some data.

    .. jupyter-execute::

        import numpy as np
        import pyobsmod as pom

        obs = ['green', 'orange', 'orange', 'red', 'red', 'red']
        mod = ['green', 'orange', 'orange', 'orange', 'red', 'red']
        labels = ['green', 'orange', 'red']
        op = pom.CategoricalDataset(obs, mod, labels)
        print(op)

    """

    def __init__(
        self,
        obs: list[str],
        mod: list[str],
        labels: list[str],
    ) -> None:
        """Initialize the CategoricalDataset object.

        Parameters
        ----------
        obs : list[str] | None
            Observations data.
        mod : list[str] | None
            Model data.
        labels : list[str]
            List of class labels.

        """
        # Initialize BaseDataframe
        super().__init__(obs, mod, None)

        # Initialize Labels
        self.labels = labels

    def __repr__(self) -> str:
        """Return a string representation of the dataset.

        Returns
        -------
        str
            A string representation of the dataset.

        """
        return "pyobsmod.CategoricalDataset(\n" + self.df.__repr__() + "\n)"

    def accuracy(self) -> float:
        """Accuracy.

        Calculates the proportion of correct predictions made by the model.

        Returns
        -------
        accuracy : float
            The accuracy of the model (between 0.0 and 1.0).

        Examples
        --------
        .. jupyter-execute::

            from pyobsmod import load_categorical_dataset_example

            ds = load_categorical_dataset_example()
            accuracy = ds.accuracy()
            print(f"{accuracy:.2f}")

        """
        # Count correct predictions
        correct_predictions = sum(
            obs == mod for obs, mod in zip(self.obs, self.mod)
        )

        # Calculate and return accuracy
        accuracy = correct_predictions / len(self.obs)
        return accuracy

    def confusion_matrix_plot(
        self,
        ax: plt.Axes | None = None,
        decimals: int = 2,
        **kwargs,
    ) -> plt.Axes:
        """Plot the confusion matrix.

        Parameters
        ----------
        ax: plt.Axes | None
            Matplotlib axis to draw the plot on.
        decimals : int
            Number of decimal places to display for statistics.
        **kwargs:
            Additional arguments that are passed to ``seaborn.heatmap``.

        Returns
        -------
        ax : matplotlib.pyplot.Axes
            The Matplotlib axes.

        Examples
        --------
        .. jupyter-execute::

            import matplotlib.pyplot as plt

            from pyobsmod import load_categorical_dataset_example

            ds = load_categorical_dataset_example()
            ds.confusion_matrix_plot()
            plt.show()

        """
        # Create ax
        if ax is None:
            _, ax = plt.subplots()

        # Calculate the confusion matrix
        confusion_mtx = confusion_matrix(
            self.df["obs"].values,
            self.df["mod"].values,
            labels=self.labels,
        )

        # Set up default arguments that can be overwritten by dictionary
        kwargs = kwargs or {}
        kwargs.setdefault("cmap", "coolwarm")
        kwargs.setdefault("annot", True)
        kwargs.setdefault("xticklabels", self.labels)
        kwargs.setdefault("yticklabels", self.labels)
        kwargs.setdefault("cbar", False)

        # Create the confusion matrix plot
        sns.heatmap(confusion_mtx.T, **kwargs)

        # Compute accuracy
        accuracy = self.accuracy()

        # Set labels and title
        ax.set_title(f"Confusion matrix, Accuracy={accuracy:.{decimals}f}")
        ax.set_ylabel("Observed label")
        ax.set_xlabel("Modelled label")

        return ax
