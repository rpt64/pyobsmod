=======
History
=======

********
Releases
********

0.1.8 (2024-09-25)
~~~~~~~~~~~~~~~~~~

New features
------------
* A new metrics module allows now for the computation of statistics without
creating a dataset object first.

0.1.7 (2024-09-24)
~~~~~~~~~~~~~~~~~~

New features
------------
* Now it is possible to pass arguments for the individual statistics to
  the function ``compute_stats`` and all the functions that rely on it.
* Formatting is now flexible and can be adjusted for each statistic.
* The ``.r2()`` method does now handle NaN values automatically.


0.1.6 (2024-06-18)
~~~~~~~~~~~~~~~~~~

New features
------------
* The ``.r2()`` method now computes the coefficient of determination instead of the correlation coefficient squared.

0.1.5 (2024-04-15)
~~~~~~~~~~~~~~~~~~

New features
------------
* Add a superclass called ``BaseDataset`` in a ``base_dataset`` module.
* Modify child class called ``Dataset``.
* Add child class ``CategoricalDataset`` in a ``categorical_dataset`` module.


0.1.4 (2024-04-10)
~~~~~~~~~~~~~~~~~~

Internal changes
----------------
* Add Dataset.stats_plot method.
* Use of ruff for formatting.

0.1.3 (2024-01-08)
~~~~~~~~~~~~~~~~~~

New features
------------
* Dataset and plot methods/functions have now an ax argument that can be used to pass already existing axes (created with for example plt.subplots).

Documentation changes
---------------------
* Add the corresponding documentation.

Credits
-------
Kuhn, Jannik.

0.1.2 (2023-12-19)
~~~~~~~~~~~~~~~~~~

New features
------------
* Add saving and loading capabilities to the Dataset object.
* Add stand-alone plots in a specific module.

Documentation changes
---------------------
* Add documentation plots tutorial.

Credits
-------
Kuhn, Jannik.

0.1.1 (2023-12-18)
~~~~~~~~~~~~~~~~~~

Documentation changes
---------------------
* First official documentation hosted on read the docs.

Credits
-------
Danglade, Nikola.

0.1.0 (2023-12-14)
~~~~~~~~~~~~~~~~~~

New features
------------
* First official realease of `pycoastalwater`.

Credits
-------
Danglade, Nikola & Kuhn, Jannik.