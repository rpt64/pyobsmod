pyobsmod
========

`pyobsmod` stands as a fundamental framework for the systematic manipulation and visualization of observed and modelled data. 

Documentation
-------------
The documentation is hosted on ReadTheDocs at https://pyobsmod.readthedocs.io/en/latest/.

Install
-------
The installation steps are described at https://pyobsmod.readthedocs.io/en/latest/getting-started-guide/installing.html

Main developers
----------------
Danglade, Nikola : nikola.danglade@suez.com 
|
Kuhn, Jannik : jannik.kuhn@univ-pau.fr
