Developer
=========

Contribute
----------

Contributions to the `pyobsmod` python package are welcome and highly appreciated. 
Join our community to fix bugs, add features, improve documentation, and enhance the 
package. Your contributions make a valuable impact and help shape the future of 
`pyobsmod`.

Guidelines
----------

`pyobsmod` Python Package Developer Guidelines:

    1 - PEP Conventions:
    When developing the `pyobsmod` Python package, it is important to adhere to the 
    guidelines outlined in PEP (Python Enhancement Proposal). Follow PEP 8 for code 
    style and formatting, which covers topics like indentation, line length, variable 
    naming, and more. Consistent adherence to PEP 8 ensures readability and 
    maintainability of the codebase.

    2 - Numpy Docstring:
    Utilize Numpy-style docstrings to document functions, classes, and modules within 
    the `pyobsmod` package. Numpy docstrings provide a structured format that 
    includes information such as parameters, return values, and examples. This format 
    helps users understand how to use the package and its functionalities effectively.

    3 - Committing Changes:
    When committing changes to the `pyobsmod` repository, it is recommended to 
    create a new branch for each feature or development task. Naming the branch as 
    "dev_feature" or following a similar naming convention helps keep track of the 
    purpose and scope of the changes being made. This practice enables better 
    collaboration, easy code review, and the ability to work on multiple features 
    concurrently.

By following these guidelines, you ensure consistency, maintainability, and readability 
of the `pyobsmod` python package. It promotes collaboration among developers and 
provides a better experience for users of the package.