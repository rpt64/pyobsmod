pyobsmod dataset
================

Import required packages.

.. jupyter-execute::

    import matplotlib.pyplot as plt
    import numpy as np
    import pyobsmod as pom

Create dataset
--------------

Create a dataset from your or some random data.

.. jupyter-execute::

    obs = np.random.normal(size=100)
    mod = np.random.normal(size=100)
    ds = pom.Dataset(obs, mod)
    print(ds)

Or if you want to be set up quickly, use an example dataset.

.. jupyter-execute::

    from pyobsmod import load_dataset_example

    ds = load_dataset_example()
    print(ds)

Manipulate dataset
------------------

It is possible to access one time step or slice it in Python fashion.

.. jupyter-execute::

    print(ds[10])
    print(ds[-10:])

Or get the length of the observations.

.. jupyter-execute::

    print(len(ds))

You can also access the data directly. Either one by one:

.. jupyter-execute::

    print(ds.obs)

or all the data together (``ds.data`` does work as well):

.. jupyter-execute::

    print(ds.values)

You can add, subtract, multiply or divide the dataset with one or multiple values
or do the same with another dataset.

For example, to add a constant bias you might do something like

.. jupyter-execute::

    print(ds + 2)

Or to add two different biases

.. jupyter-execute::

    print(ds + [2, 3])

Adding two datasets is as easy as

.. jupyter-execute::

    print(ds + ds)

Internally, all the operations are deferred to the underlying DataFrame. This means
that an operation with two datasets will be performed element-wise.
For even more control you can also access the DataFrame directly.

.. jupyter-execute::

    print(ds.df)

If you want to perform actions on the DataFrame itself, for example, taking
the mean of each column.

.. jupyter-execute::

    print(ds.df.mean())

Or adding a new column.

.. jupyter-execute::

    ds.df['new_column'] = np.random.random(100)

    print(ds)

Statistic mehods
----------------

Try out some of the statistic methods.

.. jupyter-execute::

    print(ds.compute_stats(['bias', 'rmse']))


It is also possible to pass arguments for the individual statistics. In that
case you have to pass a dictionary with the statistic as a key and the
arguments as another dictionary. For statistics without arguments, you can
simply pass an empty dictionary.

.. jupyter-execute::

    print(ds.compute_stats({'rmse': {}, 'r': {'method': 'spearman'}}))

Get a quick description of your dataset

.. jupyter-execute::

    print(ds.describe_dataset())

Get a table plot of the statistic methods.

.. jupyter-execute::

    ds.stats_plot(["bias", "rmse"])
    plt.show()

Plotting methods
----------------

Or plot your data with matplotlib or seaborn.

.. jupyter-execute::

    grid = ds.scatter_plot_sns(['bias', 'rmse', 'nrmse', 'r2'])
    fig = grid.fig
    ax = grid.ax_joint
    plt.show()

It is also possible to use the plots only as a subplot. Create
a figure and at least one axes instance and simply pass the axis
to the method!

.. jupyter-execute::

    fig, axs = plt.subplots(3, 1, figsize=(20, 10))
    # First subplot with pyobsmod
    ds.time_series_plot(ax=axs[0])
    # Second subplot with pyobsmod
    ds.scatter_plot(['bias', 'rmse', 'nrmse', 'r2'], ax=axs[1])
    # Third subplot whatever you want to plot
    axs[2].plot(obs, obs)
    plt.show()

Note however, that this does NOT work with the ``Dataset.scatter_plot_sns``
function due to the way how seaborn handles this particular plot.

Save/Load dataset
-----------------

Once you are done with working on your data, you can save it.

.. jupyter-execute::

    ds.save("my_dataset")

The next time you can load your data simply with:

.. jupyter-execute::

    ds = pom.load_dataset("my_dataset")


Working without a dataset
-------------------------

Sometimes creating an instance of a dataset is a bit of a hassle, especially
if you only want to compute some statistics quickly. In that case you can
use the standalone functions of the ``pyobsmod.metrics`` module. Here are
some examples:

.. jupyter-execute::

    import pyobsmod.metrics as pym

    obs, mod = ds.obs, ds.mod

    print(pym.rmse(obs, mod))
    print(pym.r(obs, mod, method="spearman"))
    print(pym.compute_stats(obs, mod, ["bias", "rmse"]))
    print(pym.describe_dataset(obs, mod))

