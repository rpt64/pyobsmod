"""Configuration file for the Sphinx documentation builder."""

import os
import pyobsmod
import sys

sys.path.insert(0, os.path.abspath("../.."))
package_path = os.path.abspath("../..")
os.environ["PYTHONPATH"] = ":".join((package_path, os.environ.get("PYTHONPATH", "")))

# -- Project information

project = "pyobsmod"
copyright = "2024, Danglade Nikola & Kuhn Jannik "
author = "Danglade, Nikola"


version = pyobsmod.__version__
release = pyobsmod.__version__

# -- General configuration

extensions = [
    "sphinx.ext.duration",
    "sphinx.ext.doctest",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "jupyter_sphinx",
]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3/", None),
    "sphinx": ("https://www.sphinx-doc.org/en/master/", None),
}
intersphinx_disabled_domains = ["std"]

templates_path = ["_templates"]

# -- Options for HTML output

html_logo = "_static/logo.png"
html_show_sphinx = False
pygments_style = "sphinx"
todo_include_todos = False
html_theme = "pydata_sphinx_theme"
html_theme_options = {
    "collapse_navigation": True,
    "show_nav_level": 4,
    "navbar_align": "content",
    "navbar_center": ["navbar-nav"],
    # "navbar_end": ["navbar-icon-links.html"],
    "secondary_sidebar_items": ["page-toc", "edit-this-page"],
    "icon_links": [
        {
            "name": "Gitlab",
            "url": "https://gitlab.com/kostarisk/pyobsmod",
            "icon": "fab fa-gitlab",
            "type": "fontawesome",
        }
    ],
}
html_sidebars = {
    "**": ["globaltoc.html"],
    #   "**": ["sidebar-nav-bs.html", "globaltoc.html"],
}
html_static_path = ["_static"]
html_logo = "_static/logo.png"
html_favicon = "_static/logo.png"
