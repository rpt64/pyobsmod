Main objects
============

Dataset
-------
Dataset is a class built on pandas DataFrame that allows for quick plotting
and analyzing of observational vs modelled predicted data.